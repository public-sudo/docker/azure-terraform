ARG AZURE_CLI_VERSION
ARG TERRAFORM_VERSION
ARG DEBIAN_VERSION=bullseye-20230703
# Download Terraform binary
FROM debian:${DEBIAN_VERSION} as terraform-cli
ARG TERRAFORM_VERSION=1.5.3
RUN apt-get update
RUN apt-get install --no-install-recommends -y curl=7.74.0-1.3+deb11u7 ca-certificates=20210119 unzip=6.0-26+deb11u1 gnupg=2.2.27-2+deb11u2
WORKDIR /workspace
RUN curl -Os "https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_SHA256SUMS"
RUN curl -Os "https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip"
RUN curl -Os "https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_SHA256SUMS.sig"
COPY hashicorp.asc hashicorp.asc
RUN gpg --import hashicorp.asc
RUN gpg --verify "terraform_${TERRAFORM_VERSION}_SHA256SUMS.sig" "terraform_${TERRAFORM_VERSION}_SHA256SUMS"
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN grep "terraform_${TERRAFORM_VERSION}_linux_amd64.zip" "terraform_${TERRAFORM_VERSION}_SHA256SUMS" | sha256sum -c -
RUN unzip -j "terraform_${TERRAFORM_VERSION}_linux_amd64.zip"

# Download Helm binary
FROM debian:${DEBIAN_VERSION} as helm-cli
ARG HELM_VERSION=3.12.2-1
RUN apt-get update
RUN apt-get install --no-install-recommends -y curl=7.74.0-1.3+deb11u7 ca-certificates=20210119 gnupg=2.2.27-2+deb11u2
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | tee /usr/share/keyrings/helm.gpg
RUN echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | tee /etc/apt/sources.list.d/helm-stable-debian.list
RUN apt-get update && apt-get install --no-install-recommends -y helm=${HELM_VERSION}

# Download Kubectl binary
FROM debian:${DEBIAN_VERSION} as kubectl-cli
ARG KUBECTL_VERSION=v1.27.4
RUN apt-get update
RUN apt-get install --no-install-recommends -y curl=7.74.0-1.3+deb11u7 ca-certificates=20210119 gnupg=2.2.27-2+deb11u2
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN curl -LO https://dl.k8s.io/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl
RUN curl -LO "https://dl.k8s.io/${KUBECTL_VERSION}/bin/linux/amd64/kubectl.sha256" && echo "$(cat kubectl.sha256)  kubectl" | sha256sum --check
RUN install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

# Install az CLI using PIP
FROM debian:${DEBIAN_VERSION} as azure-cli
ARG AZURE_CLI_VERSION
ARG PYTHON_MAJOR_VERSION=3.9
RUN apt-get update
RUN apt-get install -y --no-install-recommends python3=${PYTHON_MAJOR_VERSION}.2-3 python3-pip=20.3.4-4+deb11u1
RUN pip3 install --no-cache-dir azure-cli==${AZURE_CLI_VERSION}

# Build final image
FROM debian:${DEBIAN_VERSION}
LABEL maintainer="sudo@bngz.xyz"
ARG PYTHON_MAJOR_VERSION=3.9
RUN apt-get update \
  && apt-get install -y --no-install-recommends \
    ca-certificates=20210119  \
    make=4.3-4.1 \
    git=1:2.30.2-1+deb11u2 \
    python3=${PYTHON_MAJOR_VERSION}.2-3 \
    python3-distutils=${PYTHON_MAJOR_VERSION}.2-1 \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* \
  && update-alternatives --install /usr/bin/python python /usr/bin/python${PYTHON_MAJOR_VERSION} 1
WORKDIR /workspace
COPY --from=terraform-cli /workspace/terraform /usr/local/bin/terraform
COPY --from=helm-cli /usr/bin/helm /usr/bin/helm
COPY --from=kubectl-cli /usr/local/bin/kubectl /usr/local/bin
COPY --from=azure-cli /usr/local/bin/az* /usr/local/bin/
COPY --from=azure-cli /usr/local/lib/python${PYTHON_MAJOR_VERSION}/dist-packages /usr/local/lib/python${PYTHON_MAJOR_VERSION}/dist-packages
COPY --from=azure-cli /usr/lib/python3/dist-packages /usr/lib/python3/dist-packages

RUN groupadd --gid 1001 nonroot \
  # user needs a home folder to store azure credentials
  && useradd --gid nonroot --create-home --uid 1001 nonroot \
  && chown nonroot:nonroot /workspace
USER nonroot

CMD ["bash"]
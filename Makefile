AZ_VERSION ?= $(shell jq -r '.azcli_version | sort | .[-1]' supported_versions.json)
TF_VERSION ?= $(shell jq -r '.tf_version | sort | .[-1]' supported_versions.json)
HELM_VERSION ?= $(shell jq -r '.helm_version | sort | .[-1]' supported_versions.json)
KUBECTL_VERSION ?= $(shell jq -r '.helm_version | sort | .[-1]' supported_versions.json)
IMAGE_NAME=registry.gitlab.com/public-sudo/docker/azure-terraform

.PHONY: all lint build test clean

all: lint build test

lint:
	@echo "Linting Dockerfile..."
	docker run --rm --interactive --volume "${PWD}":/data --workdir /data hadolint/hadolint:2.5.0-alpine /bin/hadolint --config hadolint.yaml Dockerfile
	@echo "Dockerfile successfully linted!"

test:
	@echo "Generating test config with AZURE_CLI_VERSION=${AZ_VERSION} and TERRAFORM_VERSION=${TF_VERSION}..."
	export AZ_VERSION=${AZ_VERSION} && export TF_VERSION=${TF_VERSION}  && export TF_VERSION=${HELM_VERSIPN}
	envsubst '${AZ_VERSION},${TF_VERSION},${HELM_VERSION}' < tests/container-structure-tests.yml.template > tests/container-structure-tests.yml
	@echo "Test config successfully generated!"
	@echo "Executing container structure test..."
	docker container run --rm -it -v "${PWD}"/tests/container-structure-tests.yml:/tests.yml:ro -v /var/run/docker.sock:/var/run/docker.sock:ro gcr.io/gcp-runtimes/container-structure-test:v1.10.0 test --image ${IMAGE_NAME}:${IMAGE_TAG} --config /tests.yml

build:
	@echo "Building images with AZURE_CLI_VERSION=${AZ_VERSION} and TERRAFORM_VERSION=${TF_VERSION} and HELM_VERSION=${HELM_VERSION} and KUBECTL_VERSION=${KUBECTL_VERSION}"
	docker image build --build-arg AZURE_CLI_VERSION="${AZ_VERSION}" --build-arg TERRAFORM_VERSION="${TF_VERSION}" --build-arg HELM_VERSION="${HELM_VERSION}" --build-arg KUBECTL_VERSION="${KUBECTL_VERSION}" -t "${IMAGE_NAME}" .
	@echo "Image successfully builded!"

build-ci:
	@echo "Building images with AZURE_CLI_VERSION=${AZ_VERSION} and TERRAFORM_VERSION=${TF_VERSION} and HELM_VERSION=${HELM_VERSION} and KUBECTL_VERSION=${KUBECTL_VERSION}"
	docker image build --build-arg AZURE_CLI_VERSION="${AZ_VERSION}" --build-arg TERRAFORM_VERSION="${TF_VERSION}" --build-arg HELM_VERSION="${HELM_VERSION}" --build-arg KUBECTL_VERSION="${KUBECTL_VERSION}" -t "${IMAGE_NAME}:${CI_COMMIT_REF_SLUG}" .
	@echo "Image successfully builded!"

clean:
	@echo "Cleaning up..."
	docker image rm ${IMAGE_NAME}

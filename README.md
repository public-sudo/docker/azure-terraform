# Docker Image with Terraform, Helm, and Azure CLI

This Docker image provides a convenient environment for running Terraform, Helm, and the Azure CLI. It includes the specified versions of Terraform, Helm, and Azure CLI, along with the necessary dependencies.

## Prerequisites

Before building and using this Docker image, you must have Docker installed on your system.

## Build Instructions

On local

```bash
make lint
make build
```

This will start a bash shell inside the container, allowing you to use Terraform, Helm, and the Azure CLI.

## Version Information

The following versions of software components are included in the image:

- Terraform: `1.5.3`
- Helm: `3.12.2-1`
- Azure CLI: `2.50.0`
- Kubectl: `v1.27.4`

## Notes

- The Docker image is based on Debian ``, and it's a multi-stage build to keep the final image lightweight.

- The image is configured with a non-root user, and it is recommended to use the container as a non-root user for security purposes.

- The `WORKDIR` inside the container is set to `/workspace`, so you can mount your Terraform and Helm configurations from the host system to `/workspace` in the container.

- The Azure CLI is installed using Python pip, and the specified version is `2.50.0`.

- The image is labeled with the maintainer's email address for any inquiries or feedback.

## License

This Docker image is distributed under the terms of the MIT License. See [LICENSE](LICENSE) for more details.
